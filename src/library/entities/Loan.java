package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
    
    public static enum LoanState {CURRENT, OVER_DUE, DISCHARGED};
    
    private int loanId;
    private Book book;
    private Member member;
    private Date dueDate;
    private LoanState state;

    
    public Loan(int loanId, Book book, Member member, Date dueDate) {
        this.loanId = loanId;
        this.book = book;
        this.member = member;
        this.dueDate = dueDate;
        this.state = LoanState.CURRENT;
    }

    
    public void checkOverDue() {
        if (state == LoanState.CURRENT &&
            Calendar.getInstance().getDate().after(dueDate)) {
            this.state = LoanState.OVER_DUE;            
        }
    }

    
    public boolean isOverDue() {
        return state == LoanState.OVER_DUE;
    }

    
    public Integer getId() {
        return loanId;
    }


    public Date getDueDate() {
        return dueDate;
    }
    
    
    public String toString() {
        SimpleDateFormat dueDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        int memberId = member.getId();
        String lastName = member.getLastName();
        String firstName = member.getFirstName();
        int bookId = book.getId();
        String bookTitle = book.getTitle();
        String returnDate = dueDateFormat.format(dueDate);

        StringBuilder returnString = new StringBuilder();
        returnString.append("Loan:  ").append(loanId).append("\n")
          .append("  Borrower ").append(memberId).append(" : ")
          .append(lastName).append(", ").append(firstName).append("\n")
          .append("  Book ").append(bookId).append(" : " )
          .append(bookTitle).append("\n")
          .append("  DueDate: ").append(returnDate).append("\n")
          .append("  State: ").append(state);       
        return returnString.toString();
    }


    public Member getMember() {
        return member;
    }


    public Book getBook() {
        return book;
    }


    public void discharge() {
        state = LoanState.DISCHARGED;       
    }

}
