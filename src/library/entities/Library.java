package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {

    private static final String LIBRARY_FILE = "library.obj";
    private static final int LOAN_LIMIT = 2;
    private static final int LOAN_PERIOD = 2;
    private static final double FINE_PER_DAY = 1.0;
    private static final double MAX_FINES_OWED = 1.0;
    private static final double DAMAGE_FEE = 2.0;

    private static Library libraryInstance;
    private int bookId;
    private int memberId;
    private int loanId;
    private Date loanDate;

    private Map<Integer, Book> bookCatalog;
    private Map<Integer, Member> members;
    private Map<Integer, Loan> loans;
    private Map<Integer, Loan> currentLoans;
    private Map<Integer, Book> damagedBooks;

    private Library() {
        bookCatalog = new HashMap<>();
        members = new HashMap<>();
        loans = new HashMap<>();
        currentLoans = new HashMap<>();
        damagedBooks = new HashMap<>();
        bookId = 1;
        memberId = 1;
        loanId = 1;
    }

    public static synchronized Library getInstance() {
        if (libraryInstance == null) {
            Path filePath = Paths.get(LIBRARY_FILE);
            if (Files.exists(filePath)) {
                try (FileInputStream fileInputStream = new FileInputStream(LIBRARY_FILE);
                    ObjectInputStream libraryFile = new ObjectInputStream(fileInputStream);) {
                    libraryInstance = (Library) libraryFile.readObject();
                    Calendar.getInstance().setDate(libraryInstance.loanDate);
                    libraryFile.close();
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            else {
                libraryInstance = new Library();
            }
        }
        return libraryInstance;
    }

    public static synchronized void saveFile() {
        if (libraryInstance != null) {
            libraryInstance.loanDate = Calendar.getInstance().getDate();
            try (FileOutputStream fileOutputStream = new FileOutputStream(LIBRARY_FILE);
                ObjectOutputStream libraryFile = new ObjectOutputStream(fileOutputStream);) {
                libraryFile.writeObject(libraryInstance);
                libraryFile.flush();
                libraryFile.close();
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public int getBookId() {
        return bookId;
    }

    public int getMemberId() {
        return memberId;
    }

    private int getNextBookId() {
        return bookId++;
    }

    private int getNextMemberId() {
        return memberId++;
    }

    private int getNextLoanId() {
        return loanId++;
    }

    public List<Member> listMembers() {
        Collection<Member> memberValues = members.values();
        return new ArrayList<Member>(memberValues);
    }

    public List<Book> listBooks() {
        Collection<Book> bookCatalogValues = bookCatalog.values();
        return new ArrayList<Book>(bookCatalogValues);
    }

    public List<Loan> listCurrentLoans() {
        Collection<Loan> currentLoanValues = currentLoans.values();
        return new ArrayList<Loan>(currentLoanValues);
    }

    public Member addMember(String lastName, String firstName, String email, int phoneNo) {
        int nextMemberId = getNextMemberId();
        Member member = new Member(lastName, firstName, email, phoneNo, nextMemberId);

        int memberId = member.getId();
        members.put(memberId, member);       
        return member;
    }

    public Book addBook(String author, String title, String callNo) {
        int nextBookId = getNextBookId();
        Book book = new Book(author, title, callNo, nextBookId);

        int bookId = book.getId();
        bookCatalog.put(bookId, book);
        return book;
    }

    public Member getMember(int memberId) {
        if (members.containsKey(memberId)) {
            return members.get(memberId);
        }
        return null;
    }

    public Book getBook(int bookId) {
        if (bookCatalog.containsKey(bookId)) {
            return bookCatalog.get(bookId);
        }
        return null;
    }

    public int getLoanLimit() {
        return LOAN_LIMIT;
    }

    public boolean canMemberBorrow(Member member) {
        if (member.getNumberOfCurrentLoans() == LOAN_LIMIT) {
            return false;
        }

        if (member.finesOwed() >= MAX_FINES_OWED) {
            return false;
        }

        for (Loan loan : member.getLoans()) {
            if (loan.isOverDue()) {
                return false;
            }
        }

        return true;
    }

    public int getMemberLoansRemainingCount(Member member) {
        return LOAN_LIMIT - member.getNumberOfCurrentLoans();
    }

    public Loan issueLoan(Book book, Member member) {
        Date dueDate = Calendar. getInstance().getDueDate(LOAN_PERIOD);
        int nextLoanId = getNextLoanId();
        Loan loan = new Loan(nextLoanId, book, member, dueDate);
        member.takeOutLoan(loan);
        book.Borrow();
        int loadId = loan.getId();
        loans.put(loadId, loan);
        int bookId = book.getId();
        currentLoans.put(bookId, loan);
        return loan;
    }

    public Loan getLoanByBookId(int bookId) {
        if (currentLoans.containsKey(bookId)) {
            return currentLoans.get(bookId);
        }

        return null;
    }

    public double getOverdueFine(Loan loan) {
        if (loan.isOverDue()) {
            Date dueDate = loan.getDueDate();
            long daysOverdue = Calendar.getInstance().getDaysDifference(dueDate);
            double fine = daysOverdue * FINE_PER_DAY;
            return fine;
        }
        return 0.0;
    }

    public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
        Member member = currentLoan.getMember();
        Book book = currentLoan.getBook();

        double overdueFine = getOverdueFine(currentLoan);
        member.addFine(overdueFine);

        member.dischargeLoan(currentLoan);
        book.returnBook(isDamaged);
        int bookId = book.getId();
        if (isDamaged) {
            member.addFine(DAMAGE_FEE);
            damagedBooks.put(bookId, book);
        }
        currentLoan.discharge();
        currentLoans.remove(bookId);
    }

    public void checkCurrentLoans() {
        for (Loan loan : currentLoans.values()) {
            loan.checkOverDue();
        }
    }

    public void repairBook(Book currentBook) {
        int currentBookId = currentBook.getId();
        if (damagedBooks.containsKey(currentBookId)) {
            currentBook.repair();
            damagedBooks.remove(currentBookId);
        } else {
            throw new RuntimeException("Library: repairBook: book is not damaged");
        }
    }
}
