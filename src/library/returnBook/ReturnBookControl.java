package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

    private ReturnBookUI UI;
    private enum ControlState {INITIALISED, READY, INSPECTING};
    private ControlState state;

    private Library library;
    private Loan currentLoan;

    public ReturnBookControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUI(ReturnBookUI UI) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("ReturnBookControl: cannot call setui except in INITIALISED State");
        }

        this.UI = UI;
        UI.setState(ReturnBookUI.UIState.READY);
        state = ControlState.READY;
    }


    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY State");
        }

        Book currentBook = library.getBook(bookId);

        if (currentBook == null) {
            UI.display("Invalid Book Id");
            return;
        }
        if (!currentBook.isOnLoan()) {
            UI.display("Book has not been borrowed");
            return;
        }
        currentLoan = library.getLoanByBookId(bookId);
        double OverDueFine = 0.0;
        if (currentLoan.isOverDue()) {
            OverDueFine = library.getOverdueFine(currentLoan);
        }

        UI.display("Inspecting");
        String currentBookString = currentBook.toString();
        UI.display(currentBookString);

        if (currentLoan.isOverDue()) {
            UI.display(String.format("\nOverdue fine : $%.2f", OverDueFine));
        }

        UI.setState(ReturnBookUI.UIState.INSPECTING);
        state = ControlState.INSPECTING;
    }


    public void scanningComplete() {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY State");
        }

        UI.setState(ReturnBookUI.UIState.COMPLETED);
    }


    public void dischargeLoan(boolean IsDamaged) {
        if (!state.equals(ControlState.INSPECTING)) {
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING State");
        }

        library.dischargeLoan(currentLoan, IsDamaged);
        currentLoan = null;
        UI.setState(ReturnBookUI.UIState.READY);
        state = ControlState.READY;
    }


}
