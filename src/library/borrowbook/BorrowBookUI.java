package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {

    public static enum UserInterfaceState {INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED};

    private BorrowBookControl borrowBookControl;
    private Scanner scannerInput;
    private UserInterfaceState userInterfaceState;


    public BorrowBookUI(BorrowBookControl borrowBookControl) {
        this.borrowBookControl = borrowBookControl;
        scannerInput = new Scanner(System.in);
        userInterfaceState = UserInterfaceState.INITIALISED;
        borrowBookControl.setUI(this);
    }


    private String getInput(String userPrompt) {
        System.out.print(userPrompt);
        return scannerInput.nextLine();
    }


    private void printObject(Object outputObject) {
        System.out.println(outputObject);
    }


    public void setUserInterfaceState(UserInterfaceState userInterfaceState) {
        this.userInterfaceState = userInterfaceState;
    }


    public void run() {
        printObject("Borrow Book Use Case UI\n");

        while (true) {

            switch (userInterfaceState) {

            case CANCELLED:
                printObject("Borrowing Cancelled");
                return;

            case READY:
                String memberCardInput = getInput("Swipe member card (press <enter> to cancel): ");
                if (memberCardInput.length() == 0) {
                    borrowBookControl.cancelBorrow();
                    break;
                }
                try {
                    int memberId = Integer.valueOf(memberCardInput).intValue();
                    borrowBookControl.cardSwiped(memberId);
                } catch (NumberFormatException e) {
                    printObject("Invalid Member Id");
                }
                break;

            case RESTRICTED:
                getInput("Press <any key> to cancel");
                borrowBookControl.cancelBorrow();
                break;

            case SCANNING:
                String bookInput = getInput("Scan Book (<enter> completes): ");
                if (bookInput.length() == 0) {
                    borrowBookControl.completeBorrow();
                    break;
                }
                try {
                    int bookId = Integer.valueOf(bookInput).intValue();
                    borrowBookControl.bookScanned(bookId);

                } catch (NumberFormatException e) {
                    printObject("Invalid Book Id");
                }
                break;

            case FINALISING:
                String confirmInput = getInput("Commit loans? (Y/N): ");
                if (confirmInput.toUpperCase().equals("N")) {
                    borrowBookControl.cancelBorrow();

                }
                else {
                    borrowBookControl.commitLoans();
                    getInput("Press <any key> to complete ");
                }
                break;

            case COMPLETED:
                printObject("Borrowing Completed");
                return;

            default:
                printObject("Unhandled state");
                throw new RuntimeException("BorrowBookUI : unhandled state :" + userInterfaceState);
            }
        }
    }


    public void displayObject(Object displayObject) {
        printObject(displayObject);
    }


}
