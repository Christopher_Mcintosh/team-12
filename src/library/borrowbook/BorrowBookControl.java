package library.borrowbook;

import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {

    private BorrowBookUI borrowBookUI;

    private Library library;
    private Member member;

    private enum ControlState {INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED};

    private ControlState controlState;

    private List<Book> pendingList;
    private List<Loan> completedList;
    private Book book;


    public BorrowBookControl() {
        this.library = Library.getInstance();
        controlState = ControlState.INITIALISED;
    }


    public void setUI(BorrowBookUI borrowBookUI) {
        if (!controlState.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
        }

        this.borrowBookUI = borrowBookUI;
        borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.READY);
        controlState = ControlState.READY;
    }


    public void cardSwiped(int memberId) {
        if (!controlState.equals(ControlState.READY)) {
            throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
        }

        member = library.getMember(memberId);
        if (member == null) {
            borrowBookUI.displayObject("Invalid memberId");
            return;
        }
        if (library.canMemberBorrow(member)) {
            pendingList = new ArrayList<>();
            borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.SCANNING);
            controlState = ControlState.SCANNING;
        }
        else {
            borrowBookUI.displayObject("Member cannot borrow at this time");
            borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.RESTRICTED);
        }
    }


    public void bookScanned(int bookId) {
        book = null;
        if (!controlState.equals(ControlState.SCANNING)) {
            throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
        }

        book = library.getBook(bookId);
        if (book == null) {
            borrowBookUI.displayObject("Invalid bookId");
            return;
        }
        if (!book.isAvailable()) {
            borrowBookUI.displayObject("Book cannot be borrowed");
            return;
        }
        pendingList.add(book);
        for (Book book : pendingList) {
            String bookInfo = book.toString();
            borrowBookUI.displayObject(bookInfo);
        }

        if (library.getMemberLoansRemainingCount(member) - pendingList.size() == 0) {
            borrowBookUI.displayObject("Loan limit reached");
            completeBorrow();
        }
    }


    public void completeBorrow() {
        if (pendingList.size() == 0) {
            cancelBorrow();
        }

        else {
            borrowBookUI.displayObject("\nFinal Borrowing List");
            for (Book book : pendingList) {
                String bookInfo = book.toString();
                borrowBookUI.displayObject(bookInfo);
            }

            completedList = new ArrayList<Loan>();
            borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.FINALISING);
            controlState = ControlState.FINALISING;
        }
    }


    public void commitLoans() {
        if (!controlState.equals(ControlState.FINALISING)) {
            throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
        }

        for (Book book : pendingList) {
            Loan loan = library.issueLoan(book, member);
            completedList.add(loan);
        }
        borrowBookUI.displayObject("Completed Loan Slip");
        for (Loan loan : completedList) {
            String loanInfo = loan.toString();
            borrowBookUI.displayObject(loanInfo);
        }

        borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.COMPLETED);
        controlState = ControlState.COMPLETED;
    }


    public void cancelBorrow() {
        borrowBookUI.setUserInterfaceState(BorrowBookUI.UserInterfaceState.CANCELLED);
        controlState = ControlState.CANCELLED;
    }


}
